/*kate: tab-width 3; space-indent on; indent-width 3; replace-tabs on; eol unix; */

module whata2latex;
import whatadom;
import whatatools;

import std.array;
import std.string;
import std.conv;
import std.ascii;
import std.path;
import std.file;
import std.digest.sha;

private T max(T)(const T x, const T y) {return x > y ? x : y;}
private T min(T)(const T x, const T y) {return x < y ? x : y;}

struct whataLatexBehavior {
   bool paragraphAuthorized = true;
   bool inTitle             = false;
   bool insideTabularx      = false;
   bool insideHref          = false;
   int  titleLevel          = 0;
   int  titleOffset         = 0;
}

struct whataLatexState {
   bool    paragraphNeeded = false;
   bool    nlNeeded        = false;
   bool    blockNL         = true;
   bool    insideFootNote  = true;
   bool    explicitNL      = false;

   dchar   shortVerbChar   = '\0';
   dstring beforeContent   = "";

   bool    useAlgo         = false;
   bool    useAlltt        = false;
   bool    useColor        = false;
   bool    useFancyvrb     = false;
   bool    useFancyvrbFoot = false;
   bool    useGraphicx     = false;
   bool    useMath         = false;
   bool    useMoreVerb     = false;
   bool    useMultirow     = false;
   bool    useTabularx     = false;
   bool    useUlem         = false;
   bool    useVerbatimBox  = false;
   bool    useWrapfig      = false;
}


void whata_retrieve_use(ref whataLatexState state, const whataLatexState st) {
   state.useAlgo         = st.useAlgo;
   state.useAlltt        = st.useAlltt;
   state.useColor        = st.useColor;
   state.useFancyvrb     = st.useFancyvrb;
   state.useFancyvrbFoot = st.useFancyvrbFoot;
   state.useGraphicx     = st.useGraphicx;
   state.useMath         = st.useMath;
   state.useMoreVerb     = st.useMoreVerb;
   state.useMultirow     = st.useMultirow;
   state.useTabularx     = st.useTabularx;
   state.useUlem         = st.useUlem;
   state.useVerbatimBox  = st.useVerbatimBox;
   state.useWrapfig      = st.useWrapfig;

   state.explicitNL      = st.explicitNL;
}


dchar whata_latex_get_lstinline_char(dstring s) {
   dchar c = '!';
   while(!isPrintable(c) || s.indexOf(c) != -1) {
      ++c;
   } // FIXME theorically, could be an infinite loop
   return c;
}

dstring whata_latex_verb_number() {
   static i = 0;
   ++i;
   return "v"d ~ to!(dstring)(i);
}

void openParag(ref dstring s, const whataLatexBehavior behavior, ref whataLatexState state) {
   if(state.paragraphNeeded && behavior.paragraphAuthorized) {
      s ~="\n\\paragraph{}\n";
      state.paragraphNeeded = state.nlNeeded = false;
      state.blockNL = true;
   }
   else if(state.nlNeeded) {
      s ~= "\n\n";
      state.nlNeeded = false;
   }
}

dstring whata_latex_hack_listing_inside_tabularx(dstring s, whataLatexBehavior behavior) {
   if(behavior.insideTabularx) {
      return replace(s, "\n", "^^J\n");
   }
   else
      return s;
}

dstring whata_unlatex(dstring text) {
   text = replace(text, "\\textasciicircum ", "^");
   return replace(text, "\\textasciitilde ", "~");
}

dstring whata_latex_text(dstring text, const whataLatexBehavior behavior, ref whataLatexState state) {
   dstring[dstring] latex_special_chars = [
      "{"      : "\\{",
      "}"      : "\\}",
      "%"      : "\\%",
      "#"      : "\\#",
      "$"      : "\\$",
      "^"      : "\\textasciicircum ",
      "~"      : "\\textasciitilde ",
      "&"      : "\\&",
      "_"      : "\\_",
      "\n"     : "\\\\",
      "\u00A0" : "~"
   ];

   text = replace(text, "\\", "\\textbackslash ");
   foreach(c, r ; latex_special_chars) {
      text = replace(text, c, r);
   }
   dstring s;
   openParag(s, behavior, state);
   return s ~ text;
}

dstring whata_latex_programming_language(dstring l) {
   l = toLower(l);
   switch(l) {
      case "d":
      case "objc":
         return "[Objective] C";
      case "csharp":
         return "[Sharp] C";
      case "abap":
      case "acsl":
      case "ada":
      case "algol":
      case "ant":
      case "assembler":
      case "awk":
      case "bash":
      case "basic":
      case "c":
      case "c++":
      case "caml":
      case "cil":
      case "clean":
      case "cobol":
      case "comal80":
      case "command.com":
      case "comsol":
      case "csh":
      case "delphi":
      case "eiffel":
      case "elan":
      case "erlang":
      case "euphoria":
      case "fortran":
      case "gcl":
      case "gnuplot":
      case "haskell":
      case "html":
      case "idl":
      case "inform":
      case "java":
      case "jvmis":
      case "ksh":
      case "lingo":
      case "lisp":
      case "logo":
      case "make":
      case "mathematica":
      case "matlab":
      case "mercury":
      case "metapost":
      case "miranda":
      case "mizar":
      case "ml":
      case "modula-2":
      case "mupad":
      case "nastran":
      case "oberon-2":
      case "ocl":
      case "octave":
      case "oz":
      case "pascal":
      case "perl":
      case "php":
      case "pl/i":
      case "plasm":
      case "postscript":
      case "pov":
      case "prolog":
      case "promela":
      case "pstricks":
      case "python":
      case "r":
      case "reduce":
      case "rexx":
      case "rsl":
      case "ruby":
      case "s":
      case "sas":
      case "scilab":
      case "sh":
      case "shelxl":
      case "simula":
      case "sparql":
      case "sql":
      case "tcl":
      case "tex":
      case "vbscript":
      case "verilog":
      case "vhd":
      case "vrml":
      case "xml":
      case "xslt":
         return l;
      case "m68k":
         return "[Motorola68k] Assembler";
      case "ocaml":
      case "ocaml-brief":
         return "[Objective] Caml";
      case "asm":
         return "[x86masm] Assembler";
      case "vb":
         return "[Visual] Basic";
      case "latex":
         return "[LaTeX] TeX";
      default:
         return "";
   }
   return ""; // not supported
}

dstring whata_latex_env(dstring name, const whataNode node, const whataConf conf, whataLatexBehavior behavior, ref whataLatexState state) {
   state.paragraphNeeded = true;
   return "\\begin{" ~ name ~"}\n" ~ whata_latex_continue(node.content, conf, behavior, state) ~ "\n\\end{" ~ name ~ "}";
}

dstring whata_latex_tag(dstring name, const whataNode node, const whataConf conf, whataLatexBehavior behavior, ref whataLatexState state, dstring[] attrs = cast(dstring[])[]) {
   dstring s;
   int closingBracesNeeded;
   if(name.empty)
      closingBracesNeeded = 0;
   else {
      s ~= "\\" ~ name;
      foreach(a ; attrs) {
         s ~= "{" ~ a ~ "}";
      }
      s ~= "{";
      closingBracesNeeded = 1;

      dstring color = node.namedAttrs.get("color", "");
      if(color != "") {
         state.useColor = true;
         s ~= "{\\color{" ~ color ~"}"; // FIXME improve color handling (definecolor, ...)
         ++closingBracesNeeded;
      }
   }

   if(!node.text.empty)
      s ~= node.text;
   else
      s ~= whata_latex_continue(node.content, conf, behavior, state);
   return s ~ replicate("}"d, closingBracesNeeded);
}

dstring whata_latex_verbatim(dstring contents, ref whataLatexState state, dstring env="verbatimtab") {
   if (env == "verbatimtab") {
      state.useMoreVerb = true;
   }

   bool newVerbatim = false;
   dstring s;
   dstring nenv = env;
   while(indexOf(contents, nenv) != -1) {
      nenv ~= 'x';
      newVerbatim = true;
   }

   if(newVerbatim)
      s ~= "\\newenvironment{" ~ nenv ~ "}%\n" "{\\" ~ env ~ "}%\n" "{\\" ~ env ~ "}\n";

   whataLatexBehavior b;
   b.paragraphAuthorized = false;
   state.nlNeeded = false;

   s ~= "\\begin{" ~ nenv ~ "}\n"d ~
           contents ~
        "\\end{" ~ nenv ~ "}\n"d;

   state.paragraphNeeded = true;
   return s;
}

void whatalatex_make_title(ref dstring s, ref whataLatexState state, whataLatexBehavior behavior, ref whataLatexBehavior behaveInTitle, const whataConf conf, ref int curTitleLevel, const whataNode n) {
   state.nlNeeded = false;
   state.paragraphNeeded = true;
   curTitleLevel = behavior.titleLevel + n.level - behavior.titleOffset;

   s ~= "\n\n" ~ whata_latex_tag(replicate("sub"d, min(curTitleLevel - 1, 4)) ~ "section" ~ (n.ordered ? ""d:"*"d), n, conf, behaveInTitle, state) ~ "\n\n";
   if(curTitleLevel > 3) {
      while(isWhite(s[$-1])) {
         s.popBack();
      }
      s.popBack();
      s ~= "\\\\}";
   }
   state.blockNL = true;
}

dstring whata_latex_continue(const whataNode[] nodeList, const whataConf conf, whataLatexBehavior behavior, ref whataLatexState state) {

   whataLatexBehavior behaveInline = behavior;
   behaveInline.paragraphAuthorized = false;
   whataLatexBehavior behaveInTitle = behaveInline;
   behaveInTitle.inTitle = true;

   dstring s;
   int curTitleLevel = behavior.titleLevel;
   foreach(n ; nodeList) {
      switch(n.type) {
         case whataNodeType.Void: // nothing
            break;
         case whataNodeType.title:
            whatalatex_make_title(s, state, behavior, behaveInTitle, conf, curTitleLevel, n);
            break;
         case whataNodeType.whitespace:
            s ~= n.text;
            if(n.countNL > 1)
               state.paragraphNeeded = true;
            else if(state.blockNL) {
               state.blockNL = false;
            } else if(n.countNL == 1 && !state.explicitNL) {
               state.nlNeeded = true;
            }
            break;
         case whataNodeType.list:
            state.nlNeeded = false;

            dstring lt = n.ordered ? "enumerate" : "itemize";
            s ~= "\n\\begin{" ~ lt ~ "}" ~
                    whata_latex_continue(n.content, conf, behavior, state)
               ~ "\n\\end{" ~ lt ~ "}\n";
            state.blockNL = true;
            state.paragraphNeeded = true;
            break;
         case whataNodeType.listItem:
            state.nlNeeded = false;
            s ~= "\n\t\\item{" ~ whata_latex_continue(n.content, conf, behaveInline, state) ~ "}";
            state.blockNL = true;
            break;
         case whataNodeType.section:
            whataLatexBehavior b = behavior;
            b.titleLevel = curTitleLevel;
            b.titleOffset = 0;
            s ~= whata_latex_continue(n.content, conf, b, state);
            state.blockNL = true;
            break;
         case whataNodeType.entity:
            if(n.value == "nl")
               s ~= "\\\\";
            break;
         case whataNodeType.esc:
            s ~= whata_latex_text(n.value, behavior, state);
            break;
         case whataNodeType.table:
            size_t maxCols = 0;
            foreach(l ; n.cells) {
               maxCols = max(maxCols, l.length);
            }

            int[] cols;
            cols.length = maxCols; // NOTE: D initialize all the cells to false, and that's we want.

            state.useTabularx = true;

            s ~="\\noindent\\begin{tabularx}{\\linewidth}{|*{" ~ to!dstring(maxCols) ~ "}{X|}}\n"
                "\\hline\n";

            whataLatexBehavior b = behavior;
            b.insideTabularx      = true;
            b.paragraphAuthorized = false;

            int curRowSpan = 1;
            foreach(l ; n.cells) {
               --curRowSpan;
               int curCol = 0;
               foreach(c ; l) {
                  while(cols[curCol] > 1) {
                     s ~= " & ";
                     --cols[curCol];
                     ++curCol;
                  }

                  if(c.colspan > 1)
                     s ~="\\multicolumn{" ~ to!dstring(c.colspan) ~"}{" ~ (curCol ? ""d : "|"d) ~ "c|}{";

                  if(c.rowspan > 1) {
                     state.useMultirow = true;
                     s ~="\\multirow{" ~ to!dstring(c.rowspan) ~"}{*}{";
                  }

                  if(c.head)
                     s ~= "{\\bfseries ";

                  s ~= whata_latex_continue(c.content, conf, b, state);

                  if(c.head)
                     s ~= "}";

                  curRowSpan = max!int(curRowSpan, c.rowspan);
                  cols[curCol] = max!int(cols[curCol], c.rowspan);
                  if(c.rowspan > 1)
                     s ~="}";
                  if(c.colspan > 1)
                     s ~="}";

                  curCol += max!int(c.colspan, 1);
                  if(curCol < maxCols)
                     s ~= " & ";
               }
               s ~= "\\tabularnewline\n";
               if(curRowSpan <= 1)
                  s ~= "\\hline\n";
            }
            s ~="\\end{tabularx}\n";
            state.blockNL = true;
            state.nlNeeded = false;
            state.paragraphNeeded = true;
            break;
         case whataNodeType.cell:
            state.nlNeeded = false;
            // should never happen
            break;
         case whataNodeType.tag:
            switch(n.name) {
               case "m":
               case "math":
                  state.useMath = true;
                  openParag(s, behavior, state);
                  whataNode N = cast(whataNode) n;
                  N.text = whata_text_content(n.content);
                  s ~= "$" ~ whata_latex_tag("", N, conf, behaveInline, state) ~ "$";
                  break;
               case "M":
               case "Math":
                  state.useMath = true;
                  whataNode N = cast(whataNode) n;
                  N.text = whata_text_content(n.content);
                  s ~= "$$" ~ whata_latex_tag("", N, conf, behaveInline, state) ~ "$$";
                  state.blockNL = true;
                  break;
               case "latexEnv":
                  state.useMath = true;
                  whataNode N = cast(whataNode) n;

                  N.text = "\\begin{" ~ N.value ~ "}\n"
                           ~ whata_text_content(n.content)
                           ~ "\\end{" ~ N.value ~ "}\n";

                  s ~= whata_latex_tag("", N, conf, behaveInline, state);
                  state.blockNL = true;
                  break;
               case "latexCmd":
                  state.useMath = true;
                  whataNode N = cast(whataNode) n;

                  auto nbParam = N.unamedAttrs.length > 0
                                    ? "[" ~ whata_text_content(N.unamedAttrs[0]).strip ~ "]"
                                    : "";

                  N.text = "\\providecommand{\\" ~ N.value ~ "}" ~ nbParam ~ "{"
                           ~ whata_text_content(n.content)
                           ~ "}\n";

                  s ~= whata_latex_tag("", N, conf, behaveInline, state);
                  state.blockNL = true;
                  break;
               case "color":
                  state.useColor = true;
                  s ~= "{" ~ whata_latex_tag("color"d, n, conf, behavior, state, [n.value]) ~ "}";
                  break;
               case "pre":
                  if(!behavior.insideTabularx) {
                     state.useVerbatimBox = true;
                     dstring textcontent = whata_text_content(n.content);
                        s ~= whata_latex_verbatim(textcontent, state, "verbbox") ~ "\\theverbbox" ~ (behavior.insideTabularx ? "[t]"d : ""d) ~"\n";
   //BUG verbatim inside LaTeX cell
                     state.blockNL = true;
                     break;
                  }
               case "abstract":
                  s ~= "\\abstract{" ~ whata_latex_tag("", n, conf, behavior, state) ~ "}";
                  break;
               case "code":
                  dstring textcontent = whata_text_content(n.content);
//                   if(textcontent.indexOf("\\end{lstlisting}") == -1) {
//                      s ~= "\\lstset{language={" ~ whata_latex_programming_language(n.value) ~ "}}\n{\\begin{lstlisting}"~ ~ "\n" ~
//                           whata_latex_hack_listing_inside_tabularx(whata_text_content(n.content), behavior) ~
//                           "\n\\end{lstlisting}\n}\n";
//                   }
//                   else
//                      s ~= whata_latex_verbatim(textcontent, state);
                  string h = toHexString(sha1Of(textcontent));
                  string dir = conf.path ~ "/latexListings";
                  if(!exists(dir)) {
                     mkdirRecurse(dir);
                  }
                  string path = dir ~ "/" ~ h ~ ".tex";
                  if(exists(path)) {
                     remove(path);
                  }
                  write(path, to!string(textcontent));
                  s ~= "{\\lstinputlisting[language={" ~ whata_latex_programming_language(n.value) ~  "}" ~ (behavior.insideTabularx ? ",numbers=none"d : ""d ) ~ "]{"~ to!dstring(path) ~ "}}";
//BUG verbatim inside LaTeX cell
                  state.blockNL = true;
                  break;
               case "i":
                  openParag(s, behavior, state);
                  s ~= "{\\itshape " ~ whata_latex_tag("", n, conf, behavior, state) ~ "}";
                  break;
               case "*":
                  openParag(s, behavior, state);
                  s ~= "{\\em " ~ whata_latex_tag("", n, conf, behavior, state) ~ "}";
                  break;
               case "b":
               case "**":
                  openParag(s, behavior, state);
                  s ~= "{\\bfseries " ~ whata_latex_tag("", n, conf, behavior, state) ~ "}";
                  break;
               case "+":
               case "u":
                  openParag(s, behavior, state);
                  state.useUlem = true;
                  s ~= whata_latex_tag("uline", n, conf, behavior, state);
                  break;
               case "uu":
                  openParag(s, behavior, state);
                  state.useUlem = true;
                  s ~= whata_latex_tag("uuline", n, conf, behavior, state);
                  break;
               case "uw":
                  openParag(s, behavior, state);
                  state.useUlem = true;
                  s ~= whata_latex_tag("uwave", n, conf, behavior, state);
                  break;
               case "dotu":
                  openParag(s, behavior, state);
                  state.useUlem = true;
                  s ~= whata_latex_tag("dotuline", n, conf, behavior, state);
                  break;
               case "dashu":
                  openParag(s, behavior, state);
                  state.useUlem = true;
                  s ~= whata_latex_tag("dashuline", n, conf, behavior, state);
                  break;
               case "-":
               case "s":
                  state.useUlem = true;
                  openParag(s, behavior, state);
                  s ~= whata_latex_tag("sout", n, conf, behavior, state);
                  break;
               case "x":
                  state.useUlem = true;
                  openParag(s, behavior, state);
                  s ~= whata_latex_tag("xout", n, conf, behavior, state);
                  break;
               case "in": //user input - inline
               case "out": // program output - inline
               case "kbd": // HTML alias
               case "c":
                  openParag(s, behavior, state);
                  if(behavior.inTitle || behavior.insideHref)
                     s ~= whata_latex_text(whata_text_content(n.content), behavior, state);
                  else {
                     dstring content = whata_text_content(n.content);
                     if(behavior.insideTabularx) {
                        content = replace(content, "\\", "\\\\");
                     }

                     dchar lstinlineChar = whata_latex_get_lstinline_char(content);
                     if(behavior.insideTabularx) {
                        state.useFancyvrb = true;
                        auto v = whata_latex_verb_number();
                        if(state.shortVerbChar != '\0' && state.shortVerbChar != lstinlineChar) {
                           state.beforeContent ~= "\\UndefineShortVerb{\\"d ~ state.shortVerbChar ~ "}\n"d;
                           state.shortVerbChar = '\0';
                        }

                        if(state.shortVerbChar == '\0') {
                           state.shortVerbChar = lstinlineChar;
                           state.beforeContent ~= "\\DefineShortVerb{\\"d ~ lstinlineChar ~ "}\n"d;
                        }
                        state.beforeContent ~= "\\SaveVerb{" ~ v ~ "}"d~ [lstinlineChar] ~ content ~ [lstinlineChar] ~ "\n"d;
                        s ~= "\\UseVerb{"d ~ v ~ "}"d;
                     }
                     else {
                        if (state.insideFootNote) {
                           state.useFancyvrb = true;
                           state.useFancyvrbFoot = true;
                        }
                        s ~= "\\verb"d ~ [lstinlineChar] ~ content ~ [lstinlineChar];
                     }
                  }
                  break;
               case "mark":
                  openParag(s, behavior, state);
                  s ~= whata_latex_tag("hl", n, conf, behavior, state);
                  break;
               case "^":
                  openParag(s, behavior, state);
                  s ~= whata_latex_tag("textsuperscript", n, conf, behavior, state);
                  break;
               case "_":
                  openParag(s, behavior, state);
                  s ~= whata_latex_tag("textsubscript", n, conf, behavior, state);
                  break;
               case "q":
                  openParag(s, behavior, state);
                  s ~= whata_latex_tag("enquote", n, conf, behaveInline, state);
                  break;
               case "quote":
                  auto b = behavior;
                  b.paragraphAuthorized = false; // paragraph{} inside a quotation don't work
                  s ~= whata_latex_env("quotation", n, conf, b, state);
                  state.blockNL = true;
                  break;
               case "whata":
                  openParag(s, behavior, state);
                  s ~= "\\emph{Whata!}";
                  break;
               case "br":
                  s ~= "\\";
                  break;
               case "input":
               case "output":
               case "pcode":
                  state.useAlltt = true;
                  s ~= whata_latex_env("alltt", n, conf, behaveInline, state);
                  state.blockNL = true;
                  break;
               case "algorithmic":
                  if("title"d in n.namedAttrs) {
                     s ~= "\n\\begin{algorithm}\n\\caption{" ~ n.namedAttrs["title"d] ~ "}\n";
                  }

                  s ~= "\n\\begin{algorithmic}[1]\n" ~ whata_text_content(n.content) ~ "\n\\end{algorithmic}\n";

                  if("title"d in n.namedAttrs) {
                     s ~="\n\\end{algorithm}\n";
                  }

                  state.useAlgo = true;
                  state.blockNL = true;
                  break;
               case " ":
                  openParag(s, behavior, state);
                  dstring url;
                  whataLatexBehavior b = behaveInline;
                  b.insideHref = true;
                  dstring content = strip(whata_latex_continue(n.content, conf, b, state));
                  if(n.unamedAttrs.length == 1) {
                     url = strip(whata_text_content(n.unamedAttrs[0]));
                  }
                  else {
                     url = whata_unlatex(content);
                  }
                  whata_link(url, content, conf.lang);
                  s ~= "\\href{" ~ url ~ "}{" ~ content ~ "}";
                  break;
               case "p":
                  if(n.unamedAttrs.length == 1) {
                     if(behavior.paragraphAuthorized) {
                        s ~= "\\paragraph{" ~ whata_latex_continue(n.unamedAttrs[0], conf, behaveInline, state) ~ "}" ~ whata_latex_continue(n.content, conf, behaveInline, state);
                     }
                     else {
                        s ~= "{\\bfseries " ~ whata_latex_continue(n.unamedAttrs[0], conf, behaveInline, state) ~ " }" ~ whata_latex_continue(n.content, conf, behaveInline, state);
                     }
                  }
                  else if(behavior.paragraphAuthorized) {
                     s ~= "\\paragraph{}" ~ whata_latex_continue(n.content, conf, behaveInline, state);
                  }
                  else {
                     s ~= whata_latex_continue(n.content, conf, behaveInline, state);
                     break;
                  }
                  state.paragraphNeeded = true;
                  state.blockNL = true;
                  break;
               case "img":
                  state.useGraphicx = true;
                  if("figure"d in n.namedAttrs) {
                     s ~="\\begin{figure}[htb]"
                     "\\centering";
                  }
                  else if("float"d in n.namedAttrs) {
                     state.useWrapfig = true;
                     s ~= "\\begin{wrapfigure}{"d
                          ~ (n.namedAttrs["float"] == "right" ? "r"d : "l"d)
                          ~ "}{"d
                          ~ ("floatwidth"d in n.namedAttrs ? n.namedAttrs["floatwidth"] : "0.25\\textwidth"d)
                          ~ "}\\centering"d;
                  }
                  s ~="\\includegraphics";
                  if("scale"d in n.namedAttrs || "height"d in n.namedAttrs || "width"d in n.namedAttrs) {
                     s ~= "[";
                     if("scale"d in n.namedAttrs)
                        s ~= "scale=" ~ n.namedAttrs["scale"];
                     else {
                        auto comma = false;
                        if("height"d in n.namedAttrs) {
                           s ~= "height=" ~ n.namedAttrs["height"];
                           if(isDigit(n.namedAttrs["height"].back))
                              s~= "px";
                           comma = true;
                        }
                        if("width"d in n.namedAttrs) {
                           if(comma)
                              s ~= ',';

                           s ~= "width=" ~ n.namedAttrs["width"];
                           if(isDigit(n.namedAttrs["width"].back))
                              s~= "px";
                        }
                     }
                     s ~= "]";
                  }
                  s ~= "{{" ~ stripExtension(n.value) ~ "}."
                       ~ (extension(n.value) == ".png" ? "png"d : "eps"d) ~"}";
                  if("figure"d in n.namedAttrs) {
                     auto t = n.namedAttrs["figure"];
                     if(!t.empty)
                        s ~="\\caption{" ~ t ~ "}";

                     if("ref"d in n.namedAttrs) {
                        s ~="\\label{" ~ n.namedAttrs["ref"] ~ "}";
                     }
                     s ~="\\end{figure}";
                  }
                  else if("float"d in n.namedAttrs) {
                     s ~= "\\end{wrapfigure}";
                  }
                  break;
               case "illustr":
                  s ~= "\n\\begin{center}" ~ whata_latex_continue(n.content, conf, behavior, state) ~ "\\end{center}\n";
                  break;
               case "comment":
                  // we ignore this tag.
                  break;
               case "avoidpagebreak":
                  s ~="\n\\nopagebreak[4]\n";
                  break;
               case "newpage":
                  s ~="\n\\newpage";
                  break;
               case "ref":
                  s ~="\\ref{" ~ n.value ~ "}";
                  break;
               case "footnote":
                  state.paragraphNeeded = false;
                  whataLatexState st;
                  st.shortVerbChar = state.shortVerbChar;
                  st.insideFootNote = true;
                  whata_retrieve_use(st, state);
                  s ~= "\\footnote{" ~ whata_latex_continue(n.content, conf, behaveInline, st) ~ "}";
                  whata_retrieve_use(state, st);
                  state.beforeContent ~= st.beforeContent;
                  state.shortVerbChar = st.shortVerbChar;
                  break;
               case "part":
                  if(!n.value.empty) {
                     state.paragraphNeeded = true;
                     state.blockNL = true;
                     whataDoc doc;
                     whataConf nconf = cast(whataConf) conf;
                     nconf.path ~= dirName(to!string(n.value));
                     whata_parse(to!dstring(readText(conf.path ~ "/" ~ to!string(n.value) ~ ".whata")), nconf, doc);
                     whataLatexBehavior b = behavior;
                     if("title"d in doc.values) {
                        whataNode ntitle;
                        ntitle.type = whataNodeType.title;
                        ntitle.level = 1;
                        ntitle.ordered = true; // by default, titles are ordered
                        ntitle.content = doc.values["title"];
                        whatalatex_make_title(s, state, behavior, behaveInTitle, conf, curTitleLevel, ntitle);
                     }
                     b.titleLevel = curTitleLevel;
                     b.titleOffset = 0;
                     s ~= whata_latex_continue(doc.content, conf, b, state);
                     state.blockNL = true;
                  }
                  break;
               case "bibtex":
                  dstring bibliographystyle;
                  if("style"d in n.namedAttrs) {
                     bibliographystyle = n.namedAttrs["style"];
                  }
                  else {
                     bibliographystyle = "plain";
                  }

                  if("nocite"d in n.namedAttrs) {
                     s ~= "\\nocite{*}";
                  }

                  if(!n.value.empty) {
                     s ~= "\\bibliography{" ~ n.value ~"}{}\n\\bibliographystyle{" ~ bibliographystyle ~ "}";
                  }
                  break;
               default:
                  if(n.unamedAttrs.length == 1 && behavior.paragraphAuthorized) {
                     state.paragraphNeeded = false;
                     whataLatexState st;
                     st.shortVerbChar = state.shortVerbChar;
                     whata_retrieve_use(st, state);
                     s ~= "\\paragraph{" ~ whata_latex_continue(n.unamedAttrs[0], conf, behaveInline, st) ~ "}";
                     whata_retrieve_use(state, st);
                     state.beforeContent ~= st.beforeContent;
                     state.shortVerbChar = st.shortVerbChar;
                  }
                  s ~= whata_latex_continue(n.content, conf, behavior, state);
            }
            break;
         case whataNodeType.cdata:
            s ~= whata_latex_verbatim(n.text, state);
            state.blockNL = true;
            break;
         case whataNodeType.text:
            s ~= whata_latex_text(n.text, behavior, state);
            state.blockNL = false;
            break;
         default:
            if(n.text.empty) {
               s ~= whata_latex_continue(n.content, conf, behavior, state);
            } else {
               s ~= whata_latex_text(n.text, behavior, state);
               state.blockNL = false;
            }
            break;
      }
   }
   return s;
}

dstring whata_latex(whataDoc doc, const whataConf conf) {
   whataLatexBehavior behavior;
   whataLatexState state;
   state.explicitNL = doc.explicitNL;
   state.paragraphNeeded = behavior.paragraphAuthorized = true;
   auto tmp = whata_latex_continue(doc.content, conf, behavior, state);
   state.paragraphNeeded = behavior.paragraphAuthorized = false;

   dstring author = "", date = "", title = "";

   if("author"d in doc.values) {
      behavior.paragraphAuthorized = false;
      author = "\\author{" ~ whata_latex_continue(doc.values["author"], conf, behavior, state).strip ~ "}\n";
   }
   if("date"d in doc.values) {
      behavior.paragraphAuthorized = false;
      date = "\\date{" ~ whata_latex_continue(doc.values["date"], conf, behavior, state).strip ~ "}\n";
   }
   if("title"d in doc.values) {
      behavior.paragraphAuthorized = false;
      title = "\\title{" ~ whata_latex_continue(doc.values["title"], conf, behavior, state).strip ~ "}\n\\begin{document}\n\\maketitle\n";
   }
   else
      title = "\n\\begin{document}\n";

   dstring s =
"\\documentclass{article}";

   if (state.useUlem) {
      s ~= "\n\\usepackage[normalem]{ulem}";
   }

   if (state.useAlgo) {
      s ~= "\n\\usepackage{algpseudocode}\n"
           "\\usepackage{algorithm}";
   }

   if (state.useGraphicx) {
      s ~= "\n\\usepackage{graphicx}\n\\usepackage{epstopdf}";
   }

   if (state.useMoreVerb) {
      s ~= "\n\\usepackage{moreverb}";
   }

   if (state.useVerbatimBox) {
      s ~= "\n\\usepackage{verbatimbox}";
   }

   if (state.useFancyvrb) {
      s ~= "\n\\usepackage{fancyvrb}";
   }

   if (state.useMath || state.useAlgo) {
      s ~= "\n\\usepackage{amsmath}\n"
           "\\usepackage{amsfonts}" ~ "\\allowdisplaybreaks[1]\n";
   }

   if (state.useMath || state.useAlgo || state.useColor) {
      s ~= "\n\\usepackage{color}\n";
   }

   if (state.useAlltt) {
      s ~= "\n\\usepackage{alltt}";
   }

   if (state.useMultirow) {
      s ~= "\n\\usepackage{multirow}";
   }

   if (state.useTabularx) {
      s ~= "\n\\usepackage{tabularx}";
   }

   if (state.useWrapfig) {
      s ~= "\n\\usepackage{wrapfig}";
   }

   s ~= "
\\IfFileExists{hyperref.sty}{\\usepackage{hyperref}}{\\typeout{hyperref missing, continuing without it.}}
\\usepackage[top=2cm, bottom=2cm, left=2cm, right=2cm]{geometry}
"//\\usepackage{textcomp}
"\\usepackage{ifxetex,ifluatex}
\\let\\subsubsubsection\\paragraph
\\let\\subsubsubsubsection\\subparagraph
";

   if(("lang"d in doc.values) && (whata_text_content(doc.values["lang"]) != "english"d)) {
      s ~= "\\usepackage[" ~ whata_text_content(doc.values["lang"]) ~ ", english]{babel}";
   }
   else
      s ~= "\\usepackage[english]{babel}";

   dstring font = "";
   if ("latex-font"d in doc.values) {
      font = whata_text_content(doc.values["latex-font"]);
   }

   s ~= "\\ifxetex
      \\usepackage{fontspec,xunicode}
      \\defaultfontfeatures{Mapping=tex-text,Scale=MatchLowercase}
\\else\\ifluatex
      \\usepackage{fontspec}
   \\else
      \\usepackage[utf8x]{inputenc}
      \\usepackage[T1]{fontenc}
";

   if(font.empty) {
      s~= "\\usepackage{lmodern}
";
   }

   s ~= "
   \\fi\\fi
";

   if(!font.empty) {
      s ~= "\\usepackage{" ~ font ~ "}
";
      if (font == "times") {
         s ~= "\\ifxetex
\\setmainfont{TeX Gyre Termes}
\\fi
\\ifluatex
\\setmainfont{TeX Gyre Termes}
\\fi
";
      }
   }

   if ("latex-packages"d in doc.values) {
      foreach(p ; whata_text_content(doc.values["latex-packages"]).split(",")) {
         s ~= "\\usepackage{" ~ p.strip() ~ "}
";
      }
   }

   if ("latex-style"d in doc.values) {
      s ~= "\\usepackage{" ~ whata_text_content(doc.values["latex-style"]) ~ "}
";
   }

// WORKAROUND: incompatibility with babel, lstlisting must be declared after babel.
s ~= "
\\ifxetex
   \\usepackage{listings}
   \\lstset{breaklines=true, breakatwhitespace=true, captionpos=b, tabsize=3, showtabs=false,numbers=left,frame=single,firstnumber=1,extendedchars=true,
    inputencoding=utf8}
\\else\\ifluatex
   \\usepackage{listings}
   \\lstset{breaklines=true, breakatwhitespace=true, captionpos=b, tabsize=3, showtabs=false,numbers=left,frame=single,firstnumber=1,extendedchars=true,
    inputencoding=utf8}
\\else
   \\usepackage{listingsutf8}
   \\lstset{breaklines=true, breakatwhitespace=true, captionpos=b, tabsize=3, showtabs=false,numbers=left,frame=single,firstnumber=1,extendedchars=true,
    inputencoding=utf8/latin1}
\\fi\\fi
";

   s ~= author ~ date ~ title;

   if (state.useFancyvrbFoot) {
      s ~= "\\VerbatimFootnotes\n";
   }

   if ("showSummary"d in doc.values && whata_text_content(doc.values["showSummary"]) == "true") {
      s ~="\\tableofcontents\n";
   }

   if (state.shortVerbChar != '\0') {
      state.beforeContent ~= "\\UndefineShortVerb{\\"d ~ state.shortVerbChar ~ "}\n"d;
   }
   return s ~ "\\begin{sloppypar}\n" ~ state.beforeContent ~ tmp ~ "\\end{sloppypar}\n\\end{document}\n";
}
