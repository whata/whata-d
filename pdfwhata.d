import std.stdio;
import std.file;
import std.path;
import std.array;
import std.process;
import std.conv;


int main(string[] args) {
   if(args.length != 2) {
      writeln("Usage: " ~ args[0] ~ " file.whata\n  Makes a file.pdf PDF document from the given file.whata file using xelatex.");
      return 1;
   }

   if(exists(args[1])) {
      if(isFile(args[1])) {
         string file = args[1].replace("'", "'\"'\"'");
         string texFile = setExtension(file, "tex");
         if(system("whata -flatex -o'" ~ texFile ~ "' -i'" ~ file ~ "'")) {
            writeln("Whata to LaTeX conversion failed");
            return 1;
         }
         string outFile = setExtension(args[1], "out");
         if(exists(outFile)) {
            std.file.write(outFile, readText(outFile).replace("^", "\\136")); // Workaround https://sourceforge.net/p/xetex/bugs/85/
         }
         system("xelatex " ~ texFile);
         if(exists(outFile)) {
            std.file.write(outFile, readText(outFile).replace("^", "\\136")); // Workaround https://sourceforge.net/p/xetex/bugs/85/
         }
      }
      else {
         writeln(args[0] ~ ": " ~ args[1] ~ ": This is not a file.");
         return 1;
      }
   }
   else {
      writeln(args[0] ~ ": " ~ args[1] ~ ": No such file or directory.");
      return 1;
   }
   return 0;
}