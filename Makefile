

EXECUTABLES=whata pdfwhata
DESTDIR=/usr/local

DBG= $(shell which $(DCC))

DCC ?= gdc

ifeq ($(shell which $(DCC)), "")
	ifeq ($(shell which gdc), "")
		ifeq ($(shell which dmd), "")
			ifeq ($(shell which ldc2), "")
				DCC=ldc
			else
				DCC=ldc2
			endif
		else
			DCC=dmd
		endif
	else
		DCC=gdc
	endif
endif

ifeq ("$(shell basename $(DCC))", "gdc")
	CCTYPE?=gdc
else
	ifeq ("$(shell basename $(DCC))", "ldc2")
		CCTYPE?=ldc
	else
		ifeq ("$(shell basename $(DCC))", "ldc")
			CCTYPE?=ldc
		else
			CCTYPE?=dmd
		endif
	endif
endif

ifeq ($(CCTYPE), gdc)
	DFLAGS?= -O3 -ffunction-sections -fdata-sections -Wl,-s,--gc-sections
else
	DFLAGS?= -O -release
endif

SRC=*.d
OBJ=$(SRC:.c=.o)

all: ${EXECUTABLES}

%.o: %.d
ifeq ($(CCTYPE),gdc)
	$(DCC) ${DFLAGS} -c $^ -o $@
else
	$(DCC) ${DFLAGS} -c $^ -of$@
endif

ifeq ($(CCTYPE),gdc)
whatadomlatex.o: whatadom.d whata2latex.d
	$(DCC) ${DFLAGS} -c $^ -o $@
else
whatadomlatex.o: whatadom.o whata2latex.o
	$(LD) -r $^ -o $@
endif

whata: gdc46compat.o whatacli.o whatadomlatex.o whatatools.o json.o
ifeq ($(CCTYPE),gdc)
	$(DCC) ${DFLAGS} $^ -o $@
else
	$(DCC) ${DFLAGS} $^ -of$@
endif
	strip -s $@

pdfwhata: pdfwhata.o
ifeq ("$(CCTYPE)","gdc")
	$(DCC) ${DFLAGS} $^ -o $@
else
	$(DCC) ${DFLAGS} $^ -of$@
endif
	strip -s $@

gdc46compat.o:
ifeq ("$(CCTYPE)", "gdc")
	echo 'import std.digest.sha;' > shaPresence.d; \
	if ! ($(DCC) -c shaPresence.d -o $@); then \
	    ln -s gdc-4.6-compat/std std; \
	    $(DCC) ${DFLAGS} -c std/digest/sha.d std/digest/digest.d -o $@; \
	fi; \
	rm shaPresence.d;
else
	echo 'import std.digest.sha;' > shaPresence.d; \
	if ! ($(DCC) -c shaPresence.d -of$@); then \
		ln -s gdc-4.6-compat/std std; \
		if [ "$(CCTYPE)" = "dmd" ]; then \
			$(DCC) ${DFLAGS} -c std/digest/sha.d std/digest/digest.d -of$@; \
		else \
			$(DCC) ${DFLAGS} -c std/digest/sha.d  -ofsha.o; \
			$(DCC) ${DFLAGS} -c std/digest/digest.d -ofdigest.o; \
			$(LD) -r sha.o digest.o -o $@; \
		fi \
	fi; \
	rm shaPresence.d;
endif

clean:
	rm -fR *~ *.o ${EXECUTABLES} std

install: all
	cp ${EXECUTABLES} ${DESTDIR}/bin
