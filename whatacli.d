import whatadom;
import whata2latex;
import std.getopt;
import std.conv;
import std.stdio;
import std.file;
import std.path;
import whatatools;

void main(string[] args) {
	string fin, fout, format;
	dstring lang = "en";
	bool h;

	getopt(args,
		"in|i",     &fin,
		"out|o",    &fout,
		"format|f", &format,
		"lang|l",   &lang,
		"help|h",   &h
	);

	if(h || (!fin && !fout && !format)) {
		writeln(
			"Whata! command line interface."
			"\n -in,     -i : input file"
			"\n -out,    -o : output file"
			"\n -format, -f : output format (available formats: json,latex,plaintext)"
			"\n -lang,   -l : default language to use when needed (eg. in links)"
		);
		return;
	}

	whataConf conf;
	conf.lang = lang;
	conf.path = dirName(fin);
	whataDoc doc;

	whata_parse(to!dstring(readText(fin)), conf, doc);

	if(format == "json") {
		std.file.write(fout, to!string(to!dstring(doc) ~ "\n"d));
	} else if(format == "latex") {
		std.file.write(fout, to!string(whata_latex(doc, conf)));
	} else if(format == "plaintext") {
		std.file.write(fout, to!string(whata_text_content(doc.content)));
	} else {
		stderr.writeln("ERROR: Unknown output format");
	}
}
