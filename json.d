module json;

import std.array;
import std.traits;

S json_encode(S)(S[S] src) if(isSomeString!S) {
	S s = "{";
	S comma = "";
	foreach(k,v ; src) {
		s ~= comma ~ json_encode(k) ~ ":" ~ json_encode(v);
		if(comma.empty)
			comma = ",";
	}
	return s ~ "}";
}

S json_encode(S)(S src) if(isSomeString!S) {
   return "\"" ~ replace(
      replace(
         replace(
            replace(
               replace(
                  replace(
                     replace(src, "\\", "\\\\")
                     , "\"", "\\\"")
                  , "\f" , "\f")
               , "\r" , "\\r")
            , "\t", "\\t")
         , "\n", "\\n")
      , "\b", "\\b") ~ "\"";
}

S json_parse(S, S2)(S2 s)
{
   int i = 0;
   S s2;
   s2.length = s.length;
   while(!s.empty) {
      if(s.front == "\\") {
         s.popFront();
         assert(!s.empty);
         switch(s.front)
         {
            case 'n':
               s[++i] = '\n';
               break;
            case 'r':
               s[++i] = '\r';
               break;
            case 'b':
               s[++i] = '\b';
               break;
            case 'f':
               s[++i] = '\f';
               break;
            case 't':
               s[++i] = '\t';
               break;
            case 'u':
               assert(s.length > 5);
               static if(is(Unqual!S == string))
                  s[++i] = to!char(to!int(s[1..5]));
               else static if(is(Unqual!S == dstring))
                  s[++i] = to!dchar(to!int(s[1..5]));
               else static if(is(Unqual!S == wstring))
                  s[++i] = to!wchar(to!int(s[1..5]));
               s.popFront();
               s.popFront();
               s.popFront();
               s.popFront();
               break;
            case '"':
            case '\\':
            case '/':
               s2[++i] = s.front;
            default:
               throw new ConvException(text("bad character after '\\'"));
         }
         s.popFront();
      }
      else if(s.front == '"')
         break;
      else {
         s2[++i] = s.front;
         s.popFront();
      }
   }
   s2.length = i;

   assert(s.front == '"');
   s.popFront();
   return s2;
}