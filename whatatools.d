module whatatools;

import whatadom;
import std.array;
import std.string;

whataNode* whata_get_main_title(whataNode[] nodeList) {
	whataNode* bestTitle = null;
	bool topLevelFound = false;
	foreach(ref n ; nodeList) {
		if(n.type == whataNodeType.title) {
			if(n.level == 1) {
				if(bestTitle is null) {
					bestTitle = &n;
					topLevelFound = true;
				}
				else
					return null;  // two titles at level 1
			}
		}
		else if(n.type == whataNodeType.section && !topLevelFound) {
			if(n.containsTitle) {
				if(bestTitle is null)
					bestTitle = whata_get_main_title(n.content);
				else
					return null; // more than one sections containing titles
			}
			else
				return null; // something before the first title
		}
// 		else if(n.containsTitle) {
// 			if(bestTitle is null)
// 				bestTitle = whata_get_main_title(n.content);
// 			else if(whata_get_main_title(n.content) !is null)
// 				return null;
// 		}
		else if(bestTitle is null && n.type != whataNodeType.Void && n.type != whataNodeType.whitespace)
			return null; // something before the first title
	}
	return bestTitle;
}
//debug import std.stdio;

bool whata_tag_present(const dstring name, const whataNode[] nodes) {
	foreach(n ; nodes) {
		foreach(l ; nodes) {
			foreach(c ; nodes) {
				if(whata_tag_present(name, c.content)) {
					return true;
				}
			}
		}
		foreach(a ; n.unamedAttrs) {
			if(whata_tag_present(name, a)) {
				return true;
			}
		}

		if(whata_tag_present(name, n.content)) {
				return true;
		}
	}
	return false;
}