module whatadom;

import std.string;
import std.array;
import std.uri;
import std.conv;
import std.ascii;
import std.traits;
import json;

enum whataNodeType {
   Void,
   list,
   listItem,
   whitespace,
   title,
   table,
   cell,
   tag,
   section,
   cdata,
   text,
   entity,
   esc
}

enum whataAlignment {
   none,
   left,
   center,
   justify,
   right
}

struct whataNode {
   // global
   whataNodeType type = whataNodeType.Void;
   whataNode[] content; // cells is used instead for tables
   dstring text = ""; // corresponding text - replaces content if not empty

   // tag
   dstring name = "";
   dstring[dstring] namedAttrs;
   whataNode[][] unamedAttrs;
   dstring value = "";
   bool containsTitle = false;

   // global - parsing state
   size_t indent     = 0;
   size_t sourceChar = 1;
   size_t sourceCol  = 1;
   size_t sourceLine = 1;

   // lists and titles
   int level      = 0;
   dchar listChar;
   bool ordered   = false;

   //whitespace
   size_t countNL    = 0;

   // table
   whataNode[][] cells;

   // cell stuffs
   bool head      = false;
   int colspan   = 1;
   int rowspan   = 1;
   whataAlignment alignment = whataAlignment.none;

   // cdata
   size_t cdataCountChars;

   dstring toString() {
      dstring s = "{"
      ~ "\n\t\"type\": " ~ json_encode(to!dstring(type))
      ~ ",\n\t\"indent\": " ~ to!dstring(indent)
      ~ ",\n\t\"sourceChar\": " ~ to!dstring(sourceChar)
      ~ ",\n\t\"sourceCol\": " ~ to!dstring(sourceCol)
      ~ ",\n\t\"sourceLine\": " ~ to!dstring(sourceLine)
      ~ ",\n\t\"value\": " ~ json_encode(value);

      if(type == whataNodeType.section || type == whataNodeType.tag)
         s ~= ",\n\t\"containsTitle\": " ~ to!dstring(containsTitle);

      if(type == whataNodeType.tag) {
         s = s
         ~ ",\n\t\"name\": " ~ json_encode(name)
         ~ ",\n\t\"namedAttrs\": " ~ json_encode(namedAttrs)
         ~ ",\n\t\"unamedAttrs\": " ~ to!dstring(unamedAttrs);
      }

      if(type == whataNodeType.title || type == whataNodeType.list || type == whataNodeType.listItem) {
         s = s
            ~ ",\n\t\"level\": " ~ to!dstring(level)
            ~ ",\n\t\"ordered\": " ~ to!dstring(ordered);
      }

      if(type ==  whataNodeType.list || type == whataNodeType.listItem) {
         s ~= ",\n\t\"listChar\": \"" ~ [listChar] ~ "\"";
      }

      if(type == whataNodeType.cell) {
         s = s
         ~ ",\n\t\"head\": " ~ to!dstring(head)
         ~ ",\n\t\"colspan\": " ~ to!dstring(colspan)
         ~ ",\n\t\"rowspan\": " ~ to!dstring(rowspan)
         ~ ",\n\t\"alignment\": " ~ json_encode(to!dstring(alignment));
      }

      if(type == whataNodeType.whitespace) {
         s ~= ",\n\t\"countNL\": " ~ to!dstring(countNL);
      }

      if(type == whataNodeType.cdata) {
         s ~= ",\n\t\"cdataCountChars\": " ~ to!dstring(cdataCountChars);
      }

      if(type == whataNodeType.text || type == whataNodeType.esc || !text.empty) {
         s ~= ",\n\t\"text\": " ~ json_encode(text);
      }

      if(type == whataNodeType.tag || !content.empty) {
         s ~= ",\n\t\"content\": " ~ to!dstring(content);
      }

      if(type == whataNodeType.table || !cells.empty) {
         s ~= ",\n\t\"cells\": " ~ to!dstring(cells);
      }

      return s ~ "\n}";
   }
}

struct whataConf {
   dchar
      tagOpenChar      = '[',
      tagCloseChar     = ']',
      attrSepChar      = '|',
      dollarChar       = '$',
      escapeChar       = '~',
      equalChar        = '=',
      titleChar        = '=',
      orderedListChar  = '#',
      orderedTitleChar = '.',
      cdataOpenChar    = '<',
      cdataCloseChar   = '>',
      attrCloseChar    = '>',
      attrStartChar    = '<';

   dstring lang = "en";
   string  path;
   dstring unorderedListChars = "-*";

   whataNode[][dstring] values;
}

struct whataState {
   bool titleExplicitlyAuthorised = false;
   bool parsedTitle     = true;
   bool beginingOfLine  = true;

   size_t  curLine         = 0;
   size_t  curCol          = 0;
   size_t  curChar         = 0;
   size_t  indent          = 0;
   size_t  parentIndent    = 0;

   whataNode *curParent = null;
}

struct whataBehavior {
   dstring endChars;
}

struct whataDoc {
   whataNode[] content;
   whataNode[][dstring] values;
   bool explicitNL = false;

   dstring toString() {
      dstring s =
         "{\n"d
         "\t\"values\": {\n"d;

      char comma = 0;
      foreach(key,value; values) {
         if(comma)
            s ~= comma;
         else
            comma = ',';

          s~= "\t\t\""d ~ key ~ "\": "d ~ to!dstring(value) ~ "\n"d;
      }

      return s ~
         "\t}\n\t\"content\":"d ~ to!dstring(content) ~"\n}"d;
   }
}

private void whata_eat(ref dstring s, size_t count, ref whataState state) {
   foreach(i ; 0..count) {
      whata_pop(s, state);
   }
}

private size_t countNLDontKeep;
dstring whata_munch(ref dstring s, const dstring to_munch, ref whataState state, ref size_t countNL = countNLDontKeep) {
   dstring munched = munch(s, to_munch);
   state.curChar += munched.length;

   countNL = whata_count_nl(munched);

   state.curChar += munched.length;

   if(countNL) {
      state.indent   = whata_get_indent(munched);
      state.curCol   = munched.length - 1 - lastIndexOf(munched, '\n');
      state.curLine += countNL;
      state.beginingOfLine = (state.indent == state.curCol);
   }
   else {
      state.curCol += munched.length;
      if(state.beginingOfLine)
         state.indent += whata_get_indent(munched);

      if(!(strip(munched)).empty) // we munched non-white dcharacters
         state.beginingOfLine = false;
   }
   return munched;
}

void whata_pop(ref dstring s, ref whataState state) {
   if(s.front == '\n') {
      ++state.curLine;
      state.curCol = 0;
      state.indent = 0;
      state.beginingOfLine = true;
   }
   else {
      ++ state.curCol;
      if(state.beginingOfLine) {
         if(isWhite(s.front))
            ++state.indent;
         else
            state.beginingOfLine = false;
      }
   }

   ++ state.curChar;
   s.popFront();
}

void whata_new_node(ref whataNode node, whataNodeType type, const whataState state) {
   node.type   = type;
   node.indent = state.indent;
   node.sourceLine = state.curLine;
   node.sourceCol  = state.curCol;
   node.sourceChar = state.curChar;
}

C whata_escaped(C)(C[] s, ref whataState state) {
   Unqual!C c;
   if(s.empty)
      return '\0';
   switch(s.front)
   {
      case 'n':
         c = '\n';
         break;
      case 'r':
         c = '\r';
         break;
      case 'b':
         c = '\b';
         break;
      case 'f':
         c = '\f';
         break;
      case 't':
         c = '\t';
         break;
      case 'u':
         if(s.length > 5) {
            c = to!int(s[1..5]);
            whata_eat(s, 4, state);
         }
         else
            c = 'u';
         break;
       case '\n':
          c = '\0'; // FIXME: check correctness
          break;
      default:
         c = to!(Unqual!C)(s.front);
   }
   whata_pop(s, state);
   return c;
}

dstring whata_text_content(const whataNode[] nodeList, int indent=0) {
    dstring txtIndent = replicate(" "d, indent);
    dstring t;
    foreach(i ; 0..nodeList.length) {
        switch(nodeList[i].type) {
            case whataNodeType.whitespace:
                if(i == 0 && nodeList[i].text.indexOf('\n') != -1) {
                    dstring tmp = replace(nodeList[i].text, "\n", "\n" ~ txtIndent);
                    munch(tmp, " \t");
                    if(tmp.front == '\n')
                        tmp.popFront();
                    t ~= tmp;
                }
                else t ~= replace(nodeList[i].text, "\n", "\n" ~ txtIndent);
                break;
            case whataNodeType.text:
            case whataNodeType.cdata:
            case whataNodeType.esc:
                t ~= replace(nodeList[i].text, "\n", "\n" ~ txtIndent);
                break;
            case whataNodeType.entity:
                if(nodeList[i].value == "nl")
                    t ~= "\n" ~ txtIndent;
                break;
            case whataNodeType.tag:
            case whataNodeType.section:
            case whataNodeType.list:
            case whataNodeType.cell:
                if(nodeList[i].unamedAttrs.length == 1) {
                    t ~= strip(whata_text_content(nodeList[i].unamedAttrs[0], indent)) ~ " | ";
                }
                t ~= whata_text_content(nodeList[i].content, indent);
                break;
            case whataNodeType.listItem:
                t ~= "\n"  ~ txtIndent ~ (nodeList[i].ordered ? "#"d : "—"d) ~ " "d ~ strip(whata_text_content(nodeList[i].content, indent+3));
                break;
            case whataNodeType.table:
                //FIXME needs better rendering
                foreach(l ; nodeList[i].cells) {
                    foreach(c ; l) {
                        t ~= whata_text_content(c.content, indent);
                    }
                }
                break;
            case whataNodeType.title:
                t ~= "\n"  ~ txtIndent ~ replicate("="d, nodeList[i].level) ~ (nodeList[i].ordered ? "."d : ""d) ~ " "d ~ strip(whata_text_content(nodeList[i].content, indent)) ~ "\n";
                break;
            case whataNodeType.Void:
                break; // void element, do nothing
            default : // should not happen
                t ~= "\n"  ~ txtIndent ~ nodeList[i].text ~ strip(whata_text_content(nodeList[i].content, indent));
        }
    }
    return t;
}

dstring whata_parse_value(ref dstring s, const whataConf conf, const dstring stopChars, ref whataState state, bool quotesAllowed = false, bool whata_escape = true) {
   whata_munch(s, " \n\t\r", state);

   dstring value;
   if(quotesAllowed && !s.empty && s.front == '"') {
      whata_pop(s, state);
      if(!s.empty && s.front == '"') {
         whata_pop(s, state);
         return value; // empty value
      }
      else
         return whata_parse_value(s, conf, "\"", state, false, whata_escape);
   }
   else {
      while(!s.empty && stopChars.indexOf(s.front) == -1) {
         if(s.front == conf.escapeChar) {
            whata_pop(s, state);
            if(s.empty)
               value ~= conf.escapeChar;
            else if(whata_escape) {
               value ~= whata_escaped(s, state);
            } else {
               value ~= "~"d ~ s.front;
            }
         }
         else {
            value ~= s.front;
         }
         whata_pop(s, state);
      }
      if(!s.empty && s.front == '"')
         whata_pop(s, state);
//
      return strip(value);
   }
}

size_t whata_get_indent(const dstring s) {
   size_t index=lastIndexOf(s, '\n');
   size_t i = index + 1;
   while(i < s.length && isWhite(s[i])) {
      ++i;
   }
   return i - index - 1;
}

size_t whata_count_nl(const dstring s) {
   size_t countNL = 0;
   for(size_t i=0; i < s.length; ++i) {
      if(s[i] == '\n')
         ++countNL;
   }
   return countNL;
}

void whata_add_node(whataNode node, ref whataNode[] nodeList, ref whataState state) {
   if(node.indent != state.parentIndent) {
      whataNode *parent;
      state.curParent = null;
      whata_get_right_indented_list(state.curParent, parent, nodeList, node.indent, node.level);
      if(state.curParent !is null) {
         state.curParent = &(state.curParent.content[$ - 1]); // last item of the list
         state.parentIndent = node.indent;
      }
   }

   whataNode[] *nl;

   if(state.curParent is null) {
      nl = &nodeList;
   }
   else
      nl = &(state.curParent.content);

//    if(state.curParent is null) {
//       nodeList ~= node;
//    }
//    else
//       state.curParent.content ~= node;
   if(node.type == whataNodeType.text && nl.length > 0 && (*nl)[nl.length - 1].type == whataNodeType.text)
      (*nl)[nl.length-1].text ~= node.text;
   else
      *nl ~= node;
}

void whata_parse_whitespace(ref dstring s, const whataConf conf, ref whataNode[] nodeList, ref whataState state, const whataBehavior behavior) {
   whataNode ws;
   whata_new_node(ws, whataNodeType.whitespace, state);
   if(behavior.endChars.indexOf('\n') == -1)
      ws.text = whata_munch(s, " \t\r\n", state, ws.countNL);
   else
      ws.text = whata_munch(s, " \t\r", state, ws.countNL);

   state.curChar += ws.text.length;

//    if(ws.countNL) {
//       state.indent   = whata_get_indent(ws.text);
//       state.curCol   = state.indent;
//       state.curLine += ws.countNL;
//    }
//    else
//       state.curCol += ws.text.length;
//
//    if(ws.beginingOfLine) {
//       state.beginingOfLine = true;
//       state.indent = ws.indent;
//    }

   if(ws.text.length)
      whata_add_node(ws, nodeList, state);
}

bool whata_parse_title(ref dstring s, const whataConf conf, ref whataNode[] nodeList, ref whataState state, whataBehavior behavior) {
   if(s.empty || s.front != conf.titleChar)
      return false;

   whataNode title;
   whata_new_node(title, whataNodeType.title, state);
   title.level = 1;
   title.ordered = true; // by default, titles are ordered
   whata_pop(s, state);
   while(!s.empty && s.front == conf.titleChar) {
      whata_pop(s, state);
      ++title.level;
   }

   if(!s.empty && s.front == conf.orderedTitleChar) {
      whata_pop(s, state);
      title.ordered = false;
   }

   behavior.endChars ~= '\n';
   whata_continue_parsing(s, conf, title.content, state, behavior);
   whata_munch(s, "\n \t", state);
   state.parsedTitle = true;
   whata_add_node(title, nodeList, state);
   return true;
}

bool whata_list_char(C)(C c, const whataConf conf) {
   return c == conf.orderedListChar || conf.unorderedListChars.indexOf(c) != -1;
}

int whata_get_list_char_count(dstring s, const whataConf conf)
in {
   assert(whata_list_char(s.front, conf));
} body {
   dchar c = s.front; // we keep the first dcharacter used, we now consider others list characters as normal characters
   int count = 1;
   while(count < s.length && s[count] == c) {
      ++count;
   }
   return count;
}

void whata_get_deeper_list(ref whataNode *list, out whataNode *parent, ref int deeper) {
   if(list is null)
      return;

   assert(!list.content.empty);

   if(!list.content[$ - 1].content.empty) {
      if(list.content[$ - 1].content[$ - 1].type == whataNodeType.list) {
         list = &(list.content[$ - 1].content[$ - 1]);
         parent = list;
      }
   }
   --deeper;
}

// true corresponds to "parent must be updated"
bool whata_get_right_indented_list(ref whataNode *node, ref whataNode *parent, in whataNode[] nodeList, const size_t indent, int level) {
   if(nodeList.empty)
      return false;
   whataNode *candidate = cast(whataNode*) &(nodeList[$ - 1]);

   if(candidate.type == whataNodeType.list) {
      if(candidate.indent < indent && candidate.level <= level) {
         node = candidate;
         if(whata_get_right_indented_list(node, parent, candidate.content[$-1].content, indent, level)) {
            parent = candidate;
            return false; // do not update parent, was already done
         }
         return true; // parent should be updated by the caller
      }
   }
   return false; // no need to update parent, since no list was found
}

whataNode create_list(bool ordered, whataNode listItem, ref whataState state) {
   whataNode newList;
   whata_new_node(newList, whataNodeType.list, state);
   newList.ordered  = listItem.ordered;
   newList.level    = listItem.level;
   newList.listChar = listItem.listChar;
   newList.content ~= listItem;
   return newList;
}

bool whata_parse_list(ref dstring s, const whataConf conf, ref whataNode[] nodeList, ref whataState state, whataBehavior behavior) {
   if(s.empty || !whata_list_char(s.front, conf))
      return false;

   whataNode listItem;
   whata_new_node(listItem, whataNodeType.listItem, state);
   int listCharCount = whata_get_list_char_count(s, conf);
   listItem.level    = listCharCount;
   listItem.listChar = s.front;
   listItem.ordered  = (s.front == conf.orderedListChar);
   whataNode* list   = null,
              parent = null;

   whata_get_right_indented_list(list, parent, nodeList, listItem.indent + 1, listCharCount);

   // We enter the list that is current level + number of characters used to write this item minus 1
   int deeper = listCharCount - 1;
   while(deeper && list !is null) {
      whata_get_deeper_list(list, parent, deeper);
   }

   if(deeper)
      return false; // wrong list level, dropping item

   // we munch list characters
   while(listCharCount) {
      whata_pop(s, state);
      --listCharCount;
   }

   behavior.endChars ~= '\n';
   whata_continue_parsing(s, conf, listItem.content, state, behavior);

   if(list is null || (list.indent == listItem.indent && list.level == listItem.level && list.ordered != listItem.ordered)) {
      // we need a new list which the level is higher than any existant current list level
      // or the character of the list is different from the current list's one

      whataNode newList = create_list(listItem.ordered, listItem, state);
      if(parent is null)
         nodeList ~= newList;
      else
         parent.content[$ - 1].content ~= newList;
   }
   else {// We can simply add the item to an existing list
      if(list.indent == listItem.indent && list.level == listItem.level)
         list.content ~= listItem;
      else {// higher level, need a new list
         if(
               list.content[$-1].content.empty
            || list.content[$-1].content[$ - 1].type != whataNodeType.list
            || list.content[$-1].content[$ - 1].ordered != listItem.ordered
            || list.content[$-1].content[$ - 1].level < listItem.level
         ) {
            list.content[$-1].content ~= create_list(listItem.ordered, listItem, state);
         }
         else {
            list.content[$-1].content[$ - 1].content ~= listItem;
         }
      }
   }
   whata_munch(s, "\n \t", state);
   return true;
}

void whata_after_spaces(IntegerType)(dstring s, ref IntegerType i) {
   while(i < s.length && indexOf(" \t", s[i]) != -1) {
      ++i;
   }
}

bool whata_parse_table(ref dstring s, const whataConf conf, ref whataNode[] nodeList, ref whataState state, whataBehavior behavior) {
   if(s.front != '|')
      return false;

   whataNode table;
   whata_new_node(table, whataNodeType.table, state);

   table.cells.length = 1;

   while(!s.empty && s.front == '|') {
      whataNode cell;
      whata_new_node(cell, whataNodeType.cell, state);

      whata_pop(s, state);

      if(s.empty)
         break;

      while(s.front == '|') {
         whata_pop(s, state);
         ++cell.colspan;
      }

      if(s.empty)
         break;

      while(!s.empty && s.front == '_') {
         whata_pop(s, state);
         ++cell.rowspan;
      }

      if(s.empty)
         break;

      if(s.front == '<') {
         whata_pop(s, state);

         if(s.empty)
            break;

         if(s.front == '>') {
            cell.alignment = whataAlignment.justify;
            whata_pop(s, state);
         }
         else
            cell.alignment = whataAlignment.left;
      }
      else if(s.front == '>') {
         whata_pop(s, state);

         if(!s.empty && s.front == '<') {
            cell.alignment = whataAlignment.center;
            whata_pop(s, state);
         }
         else
            cell.alignment = whataAlignment.right;
      }

      if(!s.empty && s.front == conf.escapeChar && s.length > 2 && s[1] == ' ') {
         whata_pop(s, state);
         whata_pop(s, state);
      }
      else {
         if(!s.empty && s.front == '=') {
            cell.head = true;
            whata_pop(s, state);
         }
      }

      behavior.endChars ~= "\n|";
      whata_continue_parsing(s, conf, cell.content, state, behavior);

      table.cells[$ - 1] ~= cell;

      if(!s.empty && s.front == '|') {
         int i=1;
         whata_after_spaces(s, i);
         if(i == s.length || s[i] == '\n')
            whata_eat(s, i, state);
      }

      if(!s.empty && s.front == '\n')
      {
         whata_pop(s, state);

         int i=0;
         whata_after_spaces(s, i);
         if(i < s.length && s[i] == '|') {
            whata_eat(s, i, state);
            ++ table.cells.length;
         }
      }
   }

   whata_add_node(table, nodeList, state);
   return true;
}

bool whata_format_indentation_to_be_munched(dstring s) {
   int i=0;
   while(i < s.length) {
      if(s[i] == '\n' || !isWhite(s[i]))
         return false;
      ++i;
   }
   return true;
}

dstring whata_format_indentation(dstring s) {

   dstring old_s = s;

   while(!s.empty && isWhite(s.back) && s.back != '\n')
      s.popBack();

   size_t lastNL = lastIndexOf(s, '\n');

   if(lastNL != -1 && empty(strip(s[lastNL..$]))) {
      // multiline cdata
      size_t i=0;
      whata_after_spaces(s, i);
      if(i < s.length && s[i] == '\n') {
         if(i == lastNL)
            return s; // FIXME check that what we're doing is correct.

         s = s[i..lastNL+1]; // we forget about first spaces and we keep the last new line

         i=0;
         size_t nl = 0, minIndent = 0;
         bool notEmptyLineEncountered = false;

         while(i < s.length) { // for each line
            // i is the begining of the line
            nl = indexOf(s[i..$], '\n');
            if(nl == -1)
               break; // no more lines to read
            nl += i;
            if(!whata_format_indentation_to_be_munched(s[i..nl])) {
               size_t old_i = i;
               whata_after_spaces(s, i);
               if(!notEmptyLineEncountered || i - old_i < minIndent)
                  minIndent = i - old_i;
               notEmptyLineEncountered = true;
            }
            i = nl + 1;
         }

         s.popBack(); // we forget the last new line

         if(minIndent) {
            i=0;
            while(i < s.length) {
               nl = indexOf(s[i..$], '\n');
               if(nl == -1)
                  break;
               else {
                  nl += i;
                  i = nl + 1;
                  if(i + minIndent <= s.length && whata_format_indentation_to_be_munched(s[i..i + minIndent]))
                     s.replaceInPlace(i, i + minIndent, "");
               }
            }
         }
      }
   }
   else return old_s;

   if(!s.empty && s.front == '\n')
      s.popFront();

   return s;
}

dstring attrname(IntegerType)(dstring s, ref IntegerType i, const whataConf conf) {
   if(i >= s.length)
      return "";

   IntegerType start = i;
   if(isAlpha(s[i])) {
      ++i;
      while(i < s.length && isAlphaNum(s[i]))
         ++i;

      if(i < s.length && s[i] == conf.equalChar)
         return s[start..i];
   }
   i = start;
   return "";
}

void whata_parse_tag_content(ref dstring s, const whataConf conf, ref whataNode tag, ref whataState state, whataBehavior behavior) {
   bool oldParsedTitle = state.parsedTitle;
   state.parsedTitle = false;
   behavior.endChars =  [conf.tagCloseChar];
   whata_continue_parsing(s, conf, tag.content, state, behavior);
   if(state.parsedTitle)
      tag.containsTitle = true;
   else
      state.parsedTitle = oldParsedTitle;
}

bool whata_parse_tag(ref dstring s, const whataConf conf, ref whataNode[] nodeList, ref whataState state, whataBehavior behavior, bool stop_before_content = false) {
   if(s.front != conf.tagOpenChar)
      return false;

   whataNode tag;
   whata_new_node(tag, whataNodeType.tag, state);

   whata_pop(s, state);

   if(s.empty)
      return false;

   bool explicit_attribute_start = false;

   if(s.front == conf.attrStartChar) {
      explicit_attribute_start = true;
      whata_pop(s, state);
   }
   else if(isWhite(s.front)) { // section
      state.titleExplicitlyAuthorised = true;
      tag.type = whataNodeType.section;
      whata_pop(s, state);

      if (stop_before_content) {
         return true;
      }
      whata_parse_tag_content(s, conf, tag, state, behavior);
   }
   else {
      while(!s.empty && !isWhite(s.front) &&  s.front != conf.equalChar && s.front != conf.tagCloseChar && s.front != conf.attrCloseChar) {
         tag.name ~= s.front;
         whata_pop(s, state);

         if(!isAlphaNum(tag.name[$ - 1]) && tag.name[$ - 1] != '*')
            break; // allows writing things like that : [^2]  => will be understood like [^ 2]
      }

      if(!s.empty && s.front == conf.equalChar) {
         whata_pop(s, state);
         tag.value = whata_parse_value(s, conf, " \v\t\n]", state);
      }

      if(!s.empty && isWhite(s.front))
         whata_pop(s, state);

      size_t i=0;
      whata_after_spaces(s, i);

      if(s.empty)
         return false;

      if(s[i] == conf.attrCloseChar) {
         whata_eat(s, i+1, state);
         if(stop_before_content) {
            return true;
         }
         whata_parse_tag_content(s, conf, tag, state, behavior);
      }
      else {
         dstring attr;
         while(!(attr = attrname(s, i, conf)).empty) {
            whata_eat(s, i, state);
            whata_pop(s, state); // the equal sign
            tag.namedAttrs[attr] = whata_parse_value(s, conf, " \t\n|]", state, true);
            i=0;
            whata_after_spaces(s, i);
         }

         if(i < s.length) {
            if(s[i] == conf.attrCloseChar || s[i] == '\n') {
               whata_eat(s, i+1, state);
               if(stop_before_content) {
                  return true;
               }
               whata_parse_tag_content(s, conf, tag, state, behavior);
            }
            else if(s[i] != conf.tagCloseChar) {
               if(explicit_attribute_start)
                  behavior.endChars = [conf.tagCloseChar, conf.attrSepChar, conf.attrCloseChar];
               else
                  behavior.endChars = [conf.tagCloseChar, conf.attrSepChar];

               dstring old_s;
               whataState oldState;
               // whitespace inside s[0..i] is part of the first attribute
               while(!s.empty) {
                  if(stop_before_content) {
                     old_s = s;
                     oldState = state;
                  }
                  ++tag.unamedAttrs.length;
                  whata_continue_parsing(s, conf, tag.unamedAttrs[$ - 1], state, behavior); // FIXME: we don't handle well titles in unamed attributes. Well… who cares anyway?

                  if(s.empty || s.front != conf.attrSepChar)
                     break;
                  whata_pop(s, state);
               }

               if(!s.empty) {
                  if(explicit_attribute_start && s.front == conf.attrCloseChar) {
                     whata_pop(s, state);
                     if(stop_before_content) {
                        state = oldState;
                        s = old_s;
                        return true;
                     }
                     whata_parse_tag_content(s, conf, tag, state, behavior);
                  }
                  else if(!tag.unamedAttrs.empty) {
                     if(stop_before_content) {
                        s = old_s;
                        state = oldState;
                        return true;
                     }
                     tag.content = tag.unamedAttrs[$ - 1];
                     --tag.unamedAttrs.length;
                  }
               }
               else if(!tag.unamedAttrs.empty) {
                  if(stop_before_content) {
                     s = old_s;
                     state = oldState;
                     return true;
                  }

                  tag.content = tag.unamedAttrs[$ - 1];
                  --tag.unamedAttrs.length;
               }
            }
         }
      }

      if(tag.name == [conf.tagOpenChar])
      {
         if(1 < s.length && s[1] == conf.tagCloseChar)
            whata_pop(s, state);
         tag.name = " "; // one space codes a tag like this : [[ blah... ] (with the tag.name being conf.tagOpenChar (here '[').
         // This allow whata DOM to any format to know that the tag name was conf.tagOpenChar when the document was parsed.
         // ' ' is a forbiden tag name in Whata so no conflict can occur.
      }
   }

   if(!s.empty && s.front == conf.tagCloseChar)
      whata_pop(s, state);

   whata_add_node(tag, nodeList, state);
   return true;
}

bool whata_parse_cdata(ref dstring s, const whataConf conf, ref whataNode[] nodeList, ref whataState state) {
   int countChars=0;
   while(countChars < s.length && s[countChars] == conf.cdataOpenChar) {
      ++countChars;
   }

   if(countChars < 3)
      return false;

   whataNode cdata;
   whata_new_node(cdata, whataNodeType.cdata, state);
   whata_eat(s, countChars, state);

   size_t begining_of_the_end = 0;
   size_t i = 0;
   while(i < s.length) {
      if(s[i] == conf.cdataCloseChar) {
         begining_of_the_end = i;
         while(i < s.length && s[i] == conf.cdataCloseChar)
            ++i;

         if(i - begining_of_the_end < countChars) {
            begining_of_the_end = 0;
         } else {
            break;
         }
      }
      ++i;
   }
   if(!begining_of_the_end)
      begining_of_the_end = i;
   cdata.text = whata_format_indentation(s[0..begining_of_the_end]);
   cdata.cdataCountChars = countChars;
   whata_add_node(cdata, nodeList, state);
   whata_eat(s, i, state);
   return true;
}

void whata_link(ref dstring url, ref dstring content, const dstring defaultLang = "en") {
   size_t indexOfSpace = url.indexOf(' ');
   dstring link;
   if(indexOfSpace != -1 && indexOfSpace+1 < url.length) {
      dstring prefix = url[0..indexOfSpace];
      dstring lang;
      size_t indexOfColon = prefix.indexOf(':');
      if(indexOfColon != -1 && indexOfColon +1 < prefix.length) {
         lang = prefix[(indexOfColon+1)..$];
         prefix = prefix[0..indexOfColon];
      }
      else {
         lang = defaultLang;
      }
      if(prefix == "wp" || prefix == "wiki") {
         // Wikipedia
         link = url[(indexOfSpace+1)..$];
         url = "http://" ~ lang ~ ".wikipedia.org/wiki/" ~ to!dstring(encodeComponent(to!string(link)));
      }
      else if(prefix == "php") {
         // PHP
         url = "http://php.net/" ~ to!dstring(encodeComponent(to!string(link)));
      }
      else {
         if(to!dstring(decodeComponent(to!string(url))) == url) {
            string tmp = replace(encodeComponent(to!string(url)), "%3A", ":");
            tmp = replace(tmp, "%2F", "/");
            tmp = replace(tmp, "%23", "?");
            tmp = replace(tmp, "%5B", "#");
            tmp = replace(tmp, "%5D", "[");
            tmp = replace(tmp, "%40", "]");
            tmp = replace(tmp, "%21", "@");
            tmp = replace(tmp, "%24", "!");
            tmp = replace(tmp, "%26", "$");
            tmp = replace(tmp, "%27", "&");
            tmp = replace(tmp, "%28", "'");
            tmp = replace(tmp, "%29", "(");
            tmp = replace(tmp, "%2A", ")");
            tmp = replace(tmp, "%2B", "*");
            tmp = replace(tmp, "%2C", "+");
            tmp = replace(tmp, "%3B", ";");
            tmp = replace(tmp, "%3D", "=");
            url = to!dstring(tmp);
         }
      }
   }
   if(content.empty) {
      if(link.empty)
         content = url;
      else
         content = link;
   }
}

dstring whata_simplex2nodeName(dchar n) {
   switch(n) {
      case '*' : return "b";
      case '\'': return "i";
      case '`' : return "c";
      case '_' : return "u";
      case 's' : return "mark";
      default  : return [n];
   }
}

bool whata_parse_simplex(ref dstring s, const whataConf conf, ref whataNode[] nodeList, ref whataState state, whataBehavior behavior) {
   if(s.length < 5 || s[0] != s[1])
      return false;

   whataNode simplex;
   whata_new_node(simplex, whataNodeType.tag, state);

   if(indexOf("*'-_`\"", s.front) != -1) {
      dchar c = s.front;
      int i=2;
      while(i < s.length) {
         if(s[i] == conf.tagOpenChar) {
            whataState newState = state;
            dstring new_s = s[i..$];
            whata_parse_tag(new_s, conf, nodeList, newState, behavior, true);
            i += newState.curChar - state.curChar;
         } else if(s[i] == c && i+1 < s.length && s[i+1] == c) {
            dstring sub = s[2..i];
            whata_eat(s, 2, state);
            simplex.name = whata_simplex2nodeName(c);
            behavior.endChars = "";
            whata_continue_parsing(sub, conf, simplex.content, state, behavior);
            whata_add_node(simplex, nodeList, state);
            s = s[i-2..$];
            whata_eat(s, 2, state);
            return true;
         } else {
            ++i;
         }
      }
      return false;
   }
   else if(s[0] == '[') { // link
      int i=2;
      while(i < s.length) {
         if(i+1 < s.length && s[i] == ']' && s[i+1] == ']') {
            i+=2;
            break;
         }
         ++i;
      }

      size_t indexOfPipe = s.indexOf('|');
      if(indexOfPipe == -1)
         indexOfPipe = i;

      dstring url = s[2..indexOfPipe];
      dstring content = s[indexOfPipe..i-2];

      whata_link(url, content, conf.lang);
      whata_eat(s, indexOfPipe, state);
      simplex.name = "[";
      simplex.value = url;
      behavior.endChars = "";
      whata_continue_parsing(content, conf, simplex.content, state, behavior);
      whata_add_node(simplex, nodeList, state);
      whata_eat(s, 2, state);
      s = s[i..$];
      return true;
   }

   return false;
}

bool whata_parse_text(ref dstring s, const whataConf conf, ref whataNode[] nodeList, ref whataState state) {
   assert(s.front != '~');
   size_t i = 0;
   do   {
      ++i;
   }
   while(i < s.length && !(
         s[i] == conf.tagOpenChar
      || s[i] == conf.tagCloseChar
      || s[i] == '\n'
      || s[i] == conf.attrSepChar
      || s[i] == '*'
      || s[i] == '_'
      || s[i] == '"'
      || s[i] == '\''
      || s[i] == '-'
      || s[i] == conf.escapeChar
      || s[i] == conf.cdataOpenChar
      || s[i] == '`'
      || s[i] == conf.dollarChar
   ));
   whataNode text;
   whata_new_node(text, whataNodeType.text, state);
   text.text = s[0..i];
   whata_eat(s, i, state);
   whata_add_node(text, nodeList, state);
   return true;
}

bool whata_parse_escaped(ref dstring s, const whataConf conf, ref whataNode[] nodeList, ref whataState state) {
   if(s.length < 2 || s.front != conf.escapeChar)
      return false;
   whata_pop(s, state);

   if(s.front != '\n') { // escaping \n makes whata completely ignore it
      whataNode esc;
      if(s.front == 'n') {
         whata_new_node(esc, whataNodeType.entity, state);
         esc.value = "nl";
         esc.text = "\n";
      }
      else {
         whata_new_node(esc, whataNodeType.esc, state);
         esc.value = esc.text = [s.front];
      }
      whata_add_node(esc, nodeList, state);
   }

   whata_pop(s, state);
   return true;
}

import std.stdio;
bool whata_parse_mathDollar(ref dstring s, const whataConf conf, ref whataNode[] nodeList, ref whataState state) {
   if("mathDollar"d !in conf.values || strip(whata_text_content(conf.values["mathDollar"])) != "true" || s.length < 3 || s.front != '$')
      return false;

   whataNode simplex;
   whata_new_node(simplex, whataNodeType.tag, state);

   bool displayMath = false;

   int i=1;

   if(s[1] == conf.dollarChar) {
      displayMath = true;
      ++i;
   }

   while(i < s.length) {
      if(i+1 < s.length) {
         if(s[i] == conf.dollarChar) {
            if(displayMath) {
               if(i+1 < s.length && s[i+1] == conf.dollarChar) {
                  break;
               }
            }
            else {
               break;
            }
         }
         else if(s[i] == '\\') {
            ++i;
         }
         ++i;
      }
   }

   if(i >= s.length) {
      return false;
   }

   int dollarNumber = displayMath ? 2 : 1;
   whataNode text;
   whata_eat(s, dollarNumber, state);
   whata_new_node(text, whataNodeType.text, state);
   text.text = s[0..i-dollarNumber];
   simplex.name = displayMath ? "M" : "m";
   simplex.content ~= text;
   whata_eat(s, text.text.length, state);
   whata_add_node(simplex, nodeList, state);
   whata_eat(s, dollarNumber, state);

   return true;
}

void whata_continue_parsing(ref dstring s, const whataConf conf, ref whataNode[] nodeList, ref whataState state, const whataBehavior behavior) {
   while(!s.empty) {
      whata_parse_whitespace(s, conf, nodeList, state, behavior);

      if(s.empty)
         return;

      if(behavior.endChars.indexOf(s.front) != -1) {
         return; // we reached the end
      }

      if(!state.beginingOfLine && state.titleExplicitlyAuthorised) {
         state.beginingOfLine = whata_parse_title(s, conf, nodeList, state, behavior);
         state.titleExplicitlyAuthorised = false;
         if(s.empty)
            return;
         continue;
      }

      if(state.beginingOfLine) {
         state.beginingOfLine =
            whata_parse_list(s, conf, nodeList, state, behavior)  ||
            whata_parse_title(s, conf, nodeList, state, behavior) ||
            whata_parse_table(s, conf, nodeList, state, behavior) ;
      }
      else { // we can be at the begining of the line even after the first part of the loop
         whata_parse_cdata(s, conf, nodeList, state)              ||
         whata_parse_tag(s, conf, nodeList, state, behavior)      ||
         whata_parse_simplex(s, conf, nodeList, state, behavior)  ||
         whata_parse_escaped(s, conf, nodeList, state)            ||
         whata_parse_mathDollar(s, conf, nodeList, state)         ||
         whata_parse_text(s, conf, nodeList, state);
      }
   }
}

void whata_parse(dstring s, whataConf conf, ref whataDoc doc) {
   whataState state;
   whataBehavior behavior;

   whata_parse_whitespace(s, conf, doc.content, state, behavior);

// We try to parse the configuration of the document
   if(s.length > 5 && s.front == conf.tagOpenChar && s[1..4] == "set" && isWhite(s[4])) { // [set
      whata_eat(s, 4, state);
      dstring key, value;
      while(!s.empty && s.front != conf.tagCloseChar) { // key = value, ...
         value.length = 0;
         key = whata_parse_value(s, conf, [conf.equalChar, conf.tagCloseChar], state);
         whata_munch(s, " \n\t\r", state);
         if(s.empty)
            return;
         else if(s.front == conf.equalChar) {
            doc.values[key] = [];
            whata_pop(s, state);
            whata_munch(s, " \n\t\r", state);
            value = whata_parse_value(s, conf, [',','\n', conf.tagCloseChar], state, true, false);
            whata_continue_parsing(
               value,
               conf,
               doc.values[key],
               state,
               behavior
            );

            while(!s.empty && s.front == ',')
               whata_pop(s, state);
         }
      } // ]
      if(!s.empty && s.front == conf.tagCloseChar)
         whata_pop(s, state);
   }

   conf.values = doc.values;
   doc.explicitNL = false;
   auto p = "explicitNL"d in doc.values;
   if (p) {
		dstring val = whata_text_content(*p).strip().toLower();
		if (val != "0"d && val != "false"d) {
			doc.explicitNL = true;
		}
   }

   whata_continue_parsing(s, conf, doc.content, state, behavior);
}
